import workersService from "../../api/workersService";

const state = {
  all: [],
  workerUnderEdit: null,
  error: null
};

const actions = {
  getWorkers({ commit }) {
    workersService
      .getAll()
      .then(payload => {
        commit("setWorkers", payload);
      })
      .catch(() => {
        commit("setError", "Cannot fetch workers");
      });
  },
  deleteWorkerById({ commit }, { payload }) {
    workersService
      .removeById(payload)
      .then(() => {
        commit("removeWorkerById", payload);
      })
      .catch(() => {
        commit("setError", "Cannot remove worker");
      });
  },
  updateWorker({ commit, state }, { payload, closeModalCallback }) {
    const workerId = state.workerUnderEdit._id;

    workersService
      .updateWorker(payload, workerId)
      .then(() => {
        closeModalCallback();
        commit("updateWorker", payload);
      })
      .catch(() => {
        closeModalCallback();
        commit("setError", "Cannot update worker");
      });
  },
  addWorker({ commit }, { payload, closeModalCallback }) {
    workersService
      .addWorker(payload)
      .then(response => {
        closeModalCallback();
        commit("addWorker", response);
      })
      .catch(() => {
        closeModalCallback();
        commit("setError", "Cannot add worker");
      });
  }
};

const mutations = {
  toggleWorkerModal(state, { payload }) {
    state.isWorkerModalShown = payload;
  },
  setWorkerUnderEdit(state, { payload }) {
    state.workerUnderEdit = payload;
  },
  setWorkers(state, payload) {
    state.all = payload;
  },
  updateWorker(state, payload) {
    const workerId = state.workerUnderEdit._id;

    const updatedWorkers = state.all.map(worker => {
      if (workerId === worker._id) {
        return { ...worker, ...payload };
      } else {
        return worker;
      }
    });
    state.all = updatedWorkers;
  },
  addWorker(state, payload) {
    state.all = [...state.all, payload];
  },
  removeWorkerById(state, workerId) {
    state.all = state.all.filter(worker => worker._id !== workerId);
  },
  setError(state, error) {
    state.error = error;
  }
};

const getters = {
  workerUnderEdit: state => {
    return state.all.find(worker => worker._id === state.workerUnderEditId);
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
};
