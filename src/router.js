import Vue from "vue";
import Router from "vue-router";
import Workers from "./views/Workers";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "workers",
      component: Workers
    }
  ]
});
