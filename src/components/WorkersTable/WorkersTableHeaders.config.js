export default [
  {
    text: "Full name",
    value: "fullName",
    align: "left"
  },
  {
    text: "Birth Date",
    value: "birthDate",
    align: "left"
  },
  {
    text: "Position",
    value: "position",
    align: "left"
  },
  {
    text: "Salary, $",
    value: "salary",
    align: "left"
  },
  {
    text: "",
    value: "actions",
    sortable: false
  }
];
