# workers-tracker

```
All network requests are located in api folder
API root url is located in .env.local.example

TECHNOLOGIES USED:
vue.js
vuetify
vuex
```
## Project setup
```
npm install
rename .env.local.example to .env.local
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
