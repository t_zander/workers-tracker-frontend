import axios from "axios";

class WorkersService {
  _apiRoot = process.env.VUE_APP_API_ADDRESS;

  getAll() {
    return axios.get(`${this._apiRoot}/workers`).then(({ data }) => {
      return data.workers;
    });
  }

  removeById(workerId) {
    return axios
      .delete(`${this._apiRoot}/workers/${workerId}`)
      .then(({ data }) => {
        return data;
      });
  }

  updateWorker(worker, workerId) {
    return axios
      .patch(`${this._apiRoot}/workers/${workerId}`, { worker })
      .then(({ data }) => {
        return data;
      });
  }

  addWorker(worker) {
    return axios
      .post(`${this._apiRoot}/workers`, { worker })
      .then(({ data }) => {
        return data.worker;
      });
  }
}

export default new WorkersService();
